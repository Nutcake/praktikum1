public class Solver
{
    /**
     * Solve the equation A*x = y for x.
     *
     * @param A double[][] real NxN matrix.
     * @param y double[] real N dimensional right side vector.
     * @return double[] real N dimensional solution x.
     */
    public static double[] solve(double[][] A, double[] y)
    {
        int n = A.length;
        //Matrix.print(A);
        System.out.printf("\n");
        double[] z = new double[y.length], x = new double[n];
        Matrix.copy(z, y);
        // decompose A into left and right triangular
        // matrix A = L*R.
        try
        {
            lrdecompose2(A);
        } catch (ArithmeticException e)
        {
            System.out.println(e);
        }
        //Matrix.print(A);
        // forward elimination: R*x=:z => L*z = y => z
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                z[i] -= A[i][j] * z[j];
            }
        }
        // backward elimination: z=R*x => x
        for (int j = n - 1; j >= 0; j--)
        {
            for (int i = j + 1; i < n; i++)
            {
                z[j] -= A[j][i] * x[i];
            }
            x[j] = z[j] / A[j][j];
        }
        return x;
    }

    /**
     * Calculate the determinant of matrix A.
     *
     * @param A double[][] the NxN matrix
     * @return double the determinant of A
     */
    private static double det(double[][] A)
    {
        double sum = A[0][0];
        for (int i = 1; i < A.length; i++)
        {
            sum = sum * A[i][i];
        }
        return sum;
    }

    private static void lrdecompose(double[][] A)
    {
        double fact;
        int col, row, x;

        double[][] L = new double[A.length][A.length];

        for (col = 0; col < A.length - 1; col++)
        {
            for (row = 1 + col; row < A.length; row++)
            {
                if (A[col][col] != 0)
                {
                    fact = A[row][col] / A[col][col];
                    L[row][col] = A[row][col] / A[col][col];
                    for (x = 0; x < A.length; x++)
                    {
                        A[row][x] = A[row][x] - A[col][x] * fact;
                    }
                } else if (A[col][col] == 0)
                {
                    throw new ArithmeticException("Diagonal can't include zero");
                }
            }
        }
        System.out.println(det(A) + "\n");
        Matrix.print(A);
        System.out.printf("\n");
    }

    private static void lrdecompose2(double[][] A)
    {
        int n = A.length;
        double[][] L = new double[n][n];
        double[][] R = new double[n][n];
        double[][] sumr = new double[n][n];
        double[][] suml = new double[n][n];

        int i, k, j;

        for (i = 0; i < n; i++)
        {
            R[0][i] = A[0][i];
            L[i][0] = A[i][0] / R[0][0];
        }
        for (i = 0; i < n; i++)
        {
            if (A[i][i] == 0)
            {
                throw new ArithmeticException("Diagonal can't include zero");
            }
            for (j = 0; j < n; j++)
            {
                for (k = 0; k < i; k++)
                {
                    sumr[i][j] += L[i][k] * R[k][j];
                    suml[j][i] += L[j][k] * R[k][i];
                    R[i][j] = A[i][j] - sumr[i][j];

                    if (R[i][i] != 0)
                    {
                        L[j][i] = (A[j][i] - suml[j][i]) / R[i][i];
                    }
                }
            }
        }
        int a = 0;
        /*
        System.out.println("A input:");
        Matrix.print(A);
        System.out.println("R matrix:");
        Matrix.print(R);
        System.out.println("L matrix:");
        Matrix.print(L);

        System.out.println("combined matrix:");
        Matrix.print(A);
        System.out.println("\n" + det(R));
    */
        //A = lrcombine(L, R);
    }

    private static double[][] lrcombine(double[][] L, double[][] R)
    {
        int n = L.length;
        double[][] A = new double[n][n];
        Matrix.copy(A, R);
        int lim = 0;
        for (int col = 0; col < n; col++)
        {
            for (int row = 0; row < lim; row++)
            {
                A[col][row] = L[col][row];
            }
            lim++;
        }
        return A;
    }
}