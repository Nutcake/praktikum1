public class Matrix
{
    public static void print(double[] A)
    {
        for(int i = 0; i < A.length; i++)
        {
            System.out.printf("[" + "%.2f" + "]" + " \n", A[i]);
        }
        System.out.printf("\n");
    }

    public static void print(double[][] A)
    {
        int row, column;
        for(row=0;row<A.length;row++)
        {
            for(column=0;column<A.length;column++)
            {
                System.out.printf("[" + "%.2f" + "]" + " ", A[row][column]);
            }
            System.out.printf("\n");
        }
        System.out.printf("\n");
    }

    public static void copy(double[][] A, double[][] B)
    {
        for(int row = 0; row < A.length; row++)
        {
            for(int col = 0; col < A.length; col++)
            {
                A[row][col] = B[row][col];
            }
        }
    }

    public static void copy(double[] A, double[] B)
    {
        for (int i = 0; i < A.length; i++)
        {
            A[i] = B[i];
        }
    }


    public static double[][] add(double[][] A, double[][] B)
    {
        double[][] C = new double[A.length][A.length];
        if( A.length != B.length)
        {
            throw new IllegalArgumentException();
        }
        else
        {
            for (int j = 0; j < A.length; j++)
            {
                for (int k = 0; k < A.length; k++)
                {
                    C[j][k] = A[j][k] + B[j][k];
                }
            }
        }

        return C;
    }

    public static double[][] sub(double[][] A, double[][] B)
    {
        double[][] C = new double[A.length][A.length];
        if (A.length != B.length)
        {
            throw new IllegalArgumentException();
        } else
        {
            for (int j = 0; j < A.length; j++)
            {
                for (int k = 0; k < A.length; k++)
                {
                    C[j][k] = A[j][k] - B[j][k];
                }
            }
        }

        return C;
    }

    public static double[][] mult(double[][] A, double[][] B)
    {
        int size = A.length;
        int cX = 0;
        int cY = 0;
        int aY = 0;
        int aX = 0;
        int bY = 0;
        int bX = 0;
        int loop = 0;
        short skipFlag0 = 0;
        short skipFlag1 = 0;
        short skipFlag2 = 0;
        double sum = 0;
        double[][] C = new double[size][size];

        while(loop < size*size*size)			//loop <size>^3 times because we have to do <size> calculations for each calculated number in Matrix C and there are <size>*<size> numbers in the Matrix
        {
            sum = sum + A[aY][aX] * B[bY][bX]; 	//main multiplication stored as sum of all multiplications of that row: aX 0->2


            if(aX==size-1)						//when last column of matrix A is reached
            {									//
                C[cY][cX] = sum; 				//save the sum of the individual product for that row in the appropriate space according to cY and cX
                //
                if (cX==size-1)					//when last column of matrix C is reached
                {								//
                    cY++;						//move one row down
                    cX=0;						//start from column 0
                    skipFlag0 = 1;				//flag to skip cX++; operation when cX has already been set to 0 in this iteration of the loop
                }
                if (skipFlag0!=1)
                    cX++;
                skipFlag0 = 0;

                if (bX==size-1)					//when last column of matrix B is reached
                {
                    aY++;					//move one row down in matrix A
                    bX = 0;					//start from column 0 in matrix B
                    skipFlag1 = 1;			//flag to skip bX++; operation when bX has already been set to 0 in this iteration of the loop
                }
                //maybe combine the (cX==size-1) and (bX==size-1) checks into one operation in the future,
                if(skipFlag1!=1)				//as they have the same implications?
                    bX++;
                skipFlag1 = 0;

                sum = 0;						//reset the sum, to not add the calculations of the previous loop to the next one
                aX = 0;							//start from column 0 in matrix A
                bY = 0;							//start from row 0 in matrix B

                skipFlag2 = 1;					//flag to skip aX++; and bY++; operations when aX and bX have already been set to 0 in this iteration of the loop
            }

            if (skipFlag2!=1)
            {
                aX++;
                bY++;
            }
            skipFlag2 = 0;

            loop++;								//increase the loop counter
        }
        return C;
    }

    public static boolean isSym(double[][] A)
    {
        for (int j = 0; j < A.length; j++)
        {
            for (int k = 0; k < A.length; k++)
            {
                if (j != k)
                {
                    if (A[k][j] != A[j][k])
                        return false;
                }
            }
        }
        return true;
    }

    public static boolean isInvSym(double[][] A)
    {
        for (int j = 0; j < A.length; j++)
        {
            for (int k = 0; k < A.length; k++)
            {
                if (j == k && A[j][j] != 0)
                    return false;
                else if (A[k][j] != -(A[j][k]))
                    return false;
            }
        }
        return true;
    }
}
